# Driver for Realtek PCI-Express card reader
#
# Copyright(c) 2009 Realtek Semiconductor Corp. All rights reserved.
#
# This program is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by the
# Free Software Foundation; either version 2, or (at your option) any
# later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, see <http://www.gnu.org/licenses/>.
#
# Author:
#   wwang (wei_wang@realsil.com.cn)
#   No. 450, Shenhu Road, Suzhou Industry Park, Suzhou, China
#
# Makefile for the PCI-Express Card Reader drivers.
#

KVERSION := $(shell uname -r)

TARGET_MODULE := rts5229

EXTRA_CFLAGS := -Idrivers/scsi

obj-m += $(TARGET_MODULE).o

$(TARGET_MODULE)-objs := rtsx.o rtsx_chip.o rtsx_transport.o rtsx_scsi.o rtsx_card.o \
			 general.o sd.o ms.o

KERN_VERS := $(shell uname -r |  cut -c 3-4;)

default:
ifeq ($(shell test $(KERN_VERS) -gt 13; echo $$?),0)
	-patch -f -i ./linux-kernel-4.14.x-patch.diff
else ifeq ($(shell test $(KERN_VERS) -gt 3; echo $$?),0)
	-patch -f -i ./linux-kernel-4.x-patch.diff
endif
	cp -f ./define.release ./define.h
	make -C /lib/modules/$(shell uname -r)/build/ SUBDIRS=$(PWD) modules
	@echo now run make install as root
debug:
ifeq ($(shell test $(KERN_VERS) -gt 13; echo $$?),0)
	-patch -f -i ./linux-kernel-4.14-patch.diff
else ifeq ($(shell test $(KERN_VERS) -gt 3; echo $$?),0)
	-patch -f -i ./linux-kernel-4.x-patch.diff
endif
	cp -f ./define.debug ./define.h
	make -C /lib/modules/$(shell uname -r)/build/ SUBDIRS=$(PWD) modules
	@echo now run make install as root
install:
	cp $(TARGET_MODULE).ko ${DESTDIR}/lib/modules/$(shell uname -r)/kernel/drivers/scsi -f
	@echo now run make after-install as root
after-install:
	@echo checking the duplicates
ifneq (,$(wildcard /etc/modprobe.d/sdcard.conf))
	sed -i '/options sdhci debug_quirks2=0x40/c\options sdhci debug_quirks2=0x40' /etc/modprobe.d/sdcard.conf > /dev/null
else
	echo 'options sdhci debug_quirks2=0x40' | tee -a /etc/modprobe.d/sdcard.conf > /dev/null
endif
	grep -q '^blacklist rtsx_pci' /etc/modprobe.d/blacklist.conf && sed -i '/blacklist rtsx_pci/c\blacklist rtsx_pci' /etc/modprobe.d/blacklist.conf || echo 'blacklist rtsx_pci' >> /etc/modprobe.d/blacklist.conf
	-rmmod sdhci_acpi sdhci
	-rmmod sdhci_pci
	-modprobe sdhci
	-modprobe sdhci_pci
	depmod -a
	update-initramfs -u
ifneq (,$(wildcard /etc/pm/config.d/modules))
	sed -i '/SUSPEND_MODULES="rts5227\"/c\SUSPEND_MODULES=\"rts5227\"' /etc/modprobe.d/sdcard.conf
else
	echo SUSPEND_MODULES="rts5227" | tee -a /etc/pm/config.d/modules
endif
	@echo now please reboot your GNU/Linux machine for activating the $(TARGET_MODULE)
clean:
	rm -f *.o *.ko *.orig *.rej
	rm -f $(TARGET_MODULE).mod.c
	@echo cleaning sucessfully
