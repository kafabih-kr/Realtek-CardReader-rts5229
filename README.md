# Realtek-CardReader-rts5229
# Table of Contents
<a href="#descriptions">Descriptions<a>

<a href="#tested-device">Tested device<a>

<a href="#changelogs">Changelogs<a>

<a href="#patchlogs">Patchlogs<a>

# Descriptions
Realtek RTS5229 Linux driver kernel module.

This driver is designed for GNU/Linux systems only.

This source driver is from http://12244.wpc.azureedge.net/8012244/drivers/rtdrivers/pc/crc/0001-Realtek_RTS5229_Linux_Driver_v1.07.zip

Want to install this driver? please read <b>INSTALLATION-INSTRUCTION.txt</b>

# Tested device
Realtek Semiconductor Corp. RTS5129 Card Reader Controller
Using Linux kernel 4.4.0

# Changelogs
20/May/2018

Added patch files for Linux 4.14.x from Guglielmo Saggiorato <astyonax@gmail.com>

11/Jan/2018

Uploaded the patch files

Change the loginfo

20/Jan/2018

Now your SDCARD will detect fastest than before

Added patch files for Linux 4.4.x from Guglielmo Saggiorato <astyonax@gmail.com>

Updated makefile

Updated <b>INSTALLATION-INSTRUCTION.txt</b>

# Patchlogs
20/May/2018

Fixed __read_overflow2

11/Jan/2018

Fixed not #import <linux/vmalloc.h>

Disabled -Wno-error=date-time error messages

20/Jan/2018

Modified makefile and <b>INSTALLATION-INSTRUCTION.txt</b>

Added linux-kernel-4.x-patch.diff file
